package com.example.demoBoot;

import com.example.demoBoot.interfaces.CompactDisc;
import com.example.demoBoot.interfaces.MediaPlayer;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest

public class DemoBootApplicationTests {

    @Autowired
    private MediaPlayer cdPlayerName;

    @Autowired
    @Qualifier("metalic")
    private CompactDisc compactDisc;

    @Test
    public void contextLoads() {
        assertNotNull(compactDisc);
    }

    @Test
    public void play() {
        assertEquals("Metalica 2017 Leuzemia", cdPlayerName.play());
        assertEquals("Metalica 2017 Leuzemia", compactDisc.play());
    }
}
