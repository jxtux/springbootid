/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demoBoot.config;

import com.example.demoBoot.component.CdPlayer;
import com.example.demoBoot.component.CdPlayer2;
import com.example.demoBoot.interfaces.CompactDisc;
import com.example.demoBoot.interfaces.MediaPlayer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

@Configuration
@Profile("antonio")
@PropertySource({
    "classpath:/com/example/demoBoot/properties/app.properties",
    "classpath:/com/example/demoBoot/properties/message.properties"
})
public class CommonConfiguration {

    @Autowired
    private Environment env;

    @Bean(name = "cdPlayerName")
    public MediaPlayer cdPlayer(@Qualifier("metalic") CompactDisc cd) {
        return new CdPlayer(cd);
    }

    @Bean
    public MediaPlayer cdPlayer2() {
        return new CdPlayer2();
    }
}
