package com.example.demoBoot.component;

import com.example.demoBoot.interfaces.CompactDisc;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("rock")
public class CompactDiscRock implements CompactDisc {

    String title = "Rock 2017";
    String cantante = "Daniel F";

    @Override
    public String play() {
        return title + " " + cantante;
    }
    
    

}
