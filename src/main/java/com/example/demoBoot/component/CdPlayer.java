package com.example.demoBoot.component;

import com.example.demoBoot.interfaces.CompactDisc;
import com.example.demoBoot.interfaces.MediaPlayer;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

public class CdPlayer implements MediaPlayer {

    private CompactDisc cd;

    @Autowired
    private Environment env;

    public CdPlayer(@Qualifier("metalic")CompactDisc cd) {
        this.cd = cd;
    }

    @Override
    public String play() {
        System.out.println(env.getProperty("nameProject", ""));
        System.out.println(env.getProperty("sueldo.basico", Integer.class));
        System.out.println(env.getProperty("message.hola"));

        return cd.play();
    }

}
