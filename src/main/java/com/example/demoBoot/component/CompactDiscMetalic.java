package com.example.demoBoot.component;

import com.example.demoBoot.interfaces.CompactDisc;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Qualifier("metalic")
public class CompactDiscMetalic implements CompactDisc {

    String title = "Metalica 2017";
    String cantante = "Leuzemia";

    @Override
    public String play() {
        return title + " " + cantante;
    }

}
