package com.example.demoBoot.component;

import com.example.demoBoot.interfaces.MediaPlayer;

public class CdPlayer2 implements MediaPlayer {

    @Override
    public String play() {
        
        return "cdPlayer 2";
    }

}
