package com.example.demoBoot;

import com.example.demoBoot.config.CommonConfiguration;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;

@SpringBootApplication
//@Configuration
//@EnableAutoConfiguration
//@EnableWebMvc
//@ComponentScan("com.example.demoBoot.controller")
//@Import({ServiceConfiguration.class})
@Import({CommonConfiguration.class})

public class DemoBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoBootApplication.class, args);
    }
}
